/*
 *  SPDX-FileCopyrightText: 2021 Devin Lin <devin@kde.org>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15

/**
 * Component that triggers the opening and closing of an ActionDrawer when dragged on with touch or mouse.
 */
MouseArea {
    id: root
    
    required property ActionDrawer actionDrawer

    anchors.fill: parent

    onClicked: {
        actionDrawer.open()
    }
}
