/*
 *   SPDX-FileCopyrightText: 2021 Devin Lin <devin@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.plasma.private.inkshell 1.0 as MobileShell

import "../components" as Components
import "quicksettings"

/**
 * Root element that contains all of the ActionDrawer's contents, and is anchored to the screen.
 */
PlasmaCore.ColorScope {
    id: root
    
    required property var actionDrawer
    
    readonly property real minimizedQuickSettingsOffset: quickSettings.minimizedHeight
    readonly property real maximizedQuickSettingsOffset: minimizedQuickSettingsOffset + quickSettings.maxAddedHeight
    
    colorGroup: PlasmaCore.Theme.ViewColorGroup

    Rectangle {
        anchors.fill: parent
        color: Qt.rgba(PlasmaCore.Theme.backgroundColor.r,
                       PlasmaCore.Theme.backgroundColor.g,
                       PlasmaCore.Theme.backgroundColor.b,
                       notificationWidget.hasNotifications ? 0.95 : 0.7)
        opacity: 0.5
    }
    
    QuickSettingsDrawer {
        id: quickSettings
        z: 1 // ensure it's above notifications
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        
        actionDrawer: root.actionDrawer
    }

    MouseArea {
        z: 0
        anchors.top: quickSettings.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onClicked: actionDrawer.close()
    }
/*
    MobileShell.NotificationsWidget {
        id: notificationWidget
        anchors {
            top: quickSettings.top
            topMargin: quickSettings.height + translate.y
            bottom: parent.bottom
            bottomMargin: PlasmaCore.Units.largeSpacing
            left: parent.left
            leftMargin: PlasmaCore.Units.largeSpacing
            right: parent.right
            rightMargin: PlasmaCore.Units.largeSpacing
        }
    }*/
}
