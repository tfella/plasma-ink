# SPDX-FileCopyrightText: 2017 Marco Martin <mart@kde.org>
# SPDX-FileCopyrightText: 2021 Aleix Pol <apol@kde.org>
# SPDX-FileCopyrightText: 2020-2021 Nicolas Fella <nicolas.fella@kde.org>
# SPDX-License-Identifier: GPL-2.0-or-later

add_library(plasma_applet_inkpanel MODULE inkpanel.cpp)

kcoreaddons_desktop_to_json(plasma_applet_inkpanel package/metadata.desktop)

target_link_libraries(plasma_applet_inkpanel
                      Qt::Gui
                      Qt::DBus
                      KF5::Plasma
                      KF5::I18n
                      KF5::Service
                     )

install(TARGETS plasma_applet_inkpanel DESTINATION ${KDE_INSTALL_PLUGINDIR}/plasma/applets)
#install(FILES plasma-phonepanel-default.desktop DESTINATION ${SERVICES_INSTALL_DIR})

plasma_install_package(package org.kde.phone.panel)

